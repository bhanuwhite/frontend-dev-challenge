'use strict';
angular.module('githubApi')
        .constant('baseApiUrl', 'https://api.github.com/')
        .factory('GitHubApiCall', function ($http, $q, baseApiUrl) {
            return({
                searchRepositories: searchRepositories,
                angularCommit: angularCommit
            });

            function searchRepositories() {
                var apiUrl = baseApiUrl + 'search/repositories?q=angular+angular.js';
                var request = $http({
                    method: "GET",
                    url: apiUrl
                });
                return(request.then(handleSuccess, handleError));
            }
            function angularCommit() {
                var apiUrl = baseApiUrl + 'repos/angular/angular.js/commits'
                var request = $http({
                    method: "GET",
                    url: apiUrl
                });
                return(request.then(handleSuccess, handleError));
            }

            function handleError(response) {
                if (!angular.isObject(response.data) || !response.data.message) {
                    return($q.reject(response.data));
                }
                // Otherwise, use expected error message.
                return($q.reject(response.data.message));
            }
            function handleSuccess(data, status, headers, config) {
                return(data);
            }
        });

