'use strict';
angular.module('githubApi')
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
                $stateProvider
                        .state('search', {
                            url: '/',
                            templateUrl: 'app/modules/searchData.html',
                            controller: 'searchController'
                        });
                $locationProvider.html5Mode(true);
            }]);


