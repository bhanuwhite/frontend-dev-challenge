'use strict';
angular.module('githubApi')
        .controller('searchController', function ($scope, GitHubApiCall) {
            $scope.showData = false;
            $scope.search = function () {
                GitHubApiCall.angularCommit().then(function (commitRes) {
                    $scope.commitArr = [];
                    angular.forEach(commitRes.data, function (val, key) {
                        $scope.showData = true;
                        console.log(val);
                        var data = {
                            authorName: val.commit.author.name,
                            commitDate: val.commit.author.date,
                            commitMsg: val.commit.message
                        };
                        $scope.commitArr.push(data);
                    });
                });
            };
        });


